
# **Graph:** University CS project №3 
by: Tkachenko Nikita

### Describtion

A template implementation of the Graph class in C++. Provides user with an interface for interacting with nodes and edges, and more.

### Features
* `iterator` and related to it methods that give user an ability to iterate a graph
* `insert` family of methods that allow user to add nodes and edges
* `degree_in` and `degree_out` - for understanding how different nodes connect with each other
* Automatic Unit-Testing
* Detailed documentation

### Dependencies
* `google_tests` library
	

### Installation

You should have `git` and a compiler for C++/17 installed

```
git clone https://git.miem.hse.ru/natkachenko_1/lr3_tkachenko.git ./graph
```

#### Team

- Миколаенко Вадим Витальевич - project supervisor
- Tkachenko Nikita Andreevich - programmer
